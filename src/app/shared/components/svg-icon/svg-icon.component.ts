import {Component, Input, OnInit} from '@angular/core';
import {SvgService} from '@services';

@Component({
  selector: 'svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss']
})
export class SvgIconComponent implements OnInit {
  @Input() icon: string;
  @Input() classes = 'ico';

  constructor(
    public svg: SvgService
  ) {
  }

  ngOnInit() {
  }
}
