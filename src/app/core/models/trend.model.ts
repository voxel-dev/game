export class Trend {
  readonly type: string;
  private points: number;
  private max = 100;

  constructor(type: string, points?: number) {
    this.type = type;
    this.points = points ? points : Trend.getRandomInt(this.max * .25, this.max * .7);
  }

  private static getRandomInt(min, max): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  public getTrendType(): string {
    return this.type;
  }

  public getProgress(): number {
    return this.points / this.max * 100;
  }

  public setProgress(percent: number) {
    this.points = this.max * percent / 100;
  }

  public getMax(): number {
    return this.max;
  }

  public setPoints(points: number) {
    this.points = points;
  }

  public getPoints(): number {
    return this.points;
  }
}
