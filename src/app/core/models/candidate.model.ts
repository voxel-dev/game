import {Trend} from '@models/trend.model';

export class Candidate {
  name: string;
  group: string;
  politicalType: number;
  trends: Trend[];
  skin: string;
  votes: number;
  rating: number;
  you: boolean;

  constructor(name?) {
    if (name) {
      this.name = name;
      this.skin = 'assets/img/person-1.svg';
      this.you = true;
      this.group = 'Самовыдвиженец';
    }
  }
}
