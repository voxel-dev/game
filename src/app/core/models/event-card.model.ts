export interface EventCardResponse {
  cards: EventCard[];
}

export interface EventCard {
  id: number;
  text: string;
  img: string;
  yes?: EventCardAction;
  no?: EventCardAction;
  slogan?: number[];
  next?: number[];
}

export interface EventCardAction {
  coins: number;
  points: EventCardPoints;
  correction: EventCardCorrection;
  press: EventCardPress;
  next: number[];
}

export interface EventCardPoints {
  people: number[];
  military: number[];
  capitol: number[];
  foreign: number[];
}

export interface EventCardCorrection {
  democrat: number;
  liberal: number;
  conservator: number;
  communist: number;
}

export interface EventCardPress {
  positive: number[];
  negative: number[];
}
