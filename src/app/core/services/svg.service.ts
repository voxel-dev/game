import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {first} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SvgService {
  sprite: string;

  constructor(private http: HttpClient) {
    this.http.get('/assets/sprites/sprite.svg', {responseType: 'blob'})
      .pipe(first())
      .subscribe(sprite => {
        this.sprite = window.URL.createObjectURL(sprite);
      });
  }
}
