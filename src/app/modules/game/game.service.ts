import {Injectable} from '@angular/core';
import {Candidate} from '@models/candidate.model';
import {BehaviorSubject} from 'rxjs';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {HttpClient} from '@angular/common/http';
import {EventCard, EventCardResponse} from '@models/event-card.model';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private you: Candidate;
  private candidates: Candidate[] = [
    new Candidate(),
    new Candidate(),
    new Candidate()
  ];
  cardsSubject = new BehaviorSubject<EventCard[]>([]);
  sloganSubject = new BehaviorSubject<EventCard[]>([]);
  private cards: EventCard[] = [];
  private slogans = [];

  private press = [];

  private days = 30;
  private coins = 1000;
  private political: string;
  politicalTypes = ['Демократ', 'Комунист', 'Либерал', 'Консерватор'];

  endGame = false;

  constructor(
    private httpClient: HttpClient
  ) {
    this.createYou();
    this.subscribeCardsData();
    this.subscribeSlogansData();
  }

  destroy() {
  }

  private subscribeCardsData() {
    this.httpClient.get('assets/data/cards-data.json')
      .pipe(untilDestroyed(this, 'destroy'))
      .subscribe((data: EventCardResponse) => {
        this.cards = data.cards;
        this.cardsSubject.next(this.cards);
      });
  }

  private subscribeSlogansData() {
    this.httpClient.get('assets/data/slogan-data.json')
      .pipe(untilDestroyed(this, 'destroy'))
      .subscribe((data) => {
        // @ts-ignore
        this.slogans = data.slogans;
        this.sloganSubject.next(this.slogans);
      });
  }

  getCandidates(): Candidate[] {
    return this.candidates;
  }

  private createYou() {
    this.you = new Candidate('Это Вы');
    this.candidates.unshift(this.you);
  }

  updateCandidate(ref: Candidate, update: Candidate) {
    const index = this.candidates.indexOf(ref);
    this.candidates[index] = update;
    this.you = this.candidates[0];
    this.initPolitical();
  }

  private initPolitical() {
    this.political = this.politicalTypes[this.candidates[0].politicalType];
  }

  getPolitical(): string {
    return this.political;
  }

  getYou(): Candidate {
    return this.you;
  }

  getPress() {
    return this.press;
  }

  getCoins(): number {
    return this.coins;
  }

  setCoins(coins: number) {
    this.coins = coins;
  }

  getDays(): number {
    return this.days;
  }

  setDays(days: number) {
    this.days = days;
  }

  getCardById(id: number): EventCard {
    return this.cards.find((card: EventCard) => card.id === id);
  }

  getSlogans() {
    return this.slogans;
  }
}
