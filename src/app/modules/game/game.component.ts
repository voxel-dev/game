import {AfterViewChecked, Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {GameService} from '@app/modules/game/game.service';
import {Candidate} from '@models/candidate.model';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  you: Candidate;
  candidates: Candidate[];

  constructor(
    private gameService: GameService
  ) {
  }

  ngOnInit() {
    this.candidates = this.gameService.getCandidates();
    this.you = this.gameService.getYou();
  }
}
