import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EventCard} from '@models/event-card.model';
import {GameService} from '@app/modules/game/game.service';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent implements OnInit, OnDestroy {
  @ViewChild('cardRef', {static: false}) cardRef: ElementRef;
  card: EventCard;
  @Input() set setData(data: EventCard) {
    this.card = data;
  }

  constructor(
    private gameService: GameService
  ) {
  }

  ngOnInit() {
    this.replacePolitical();
  }

  ngOnDestroy(): void {
  }

  getRef() {
    return this.cardRef;
  }

  replacePolitical() {
    this.card.text = this.card.text.replace('%political%', this.gameService.getPolitical());
  }
}
