import {Component, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {EventCardComponent} from '@app/modules/game/components/console/event-card/event-card.component';
import {GameService} from '@app/modules/game/game.service';
import {EventCard, EventCardAction} from '@models/event-card.model';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {MathClass} from '@models/math.class';
import {SloganCardComponent} from '@app/modules/game/components/console/slogan-card/slogan-card.component';

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.scss']
})
export class ConsoleComponent extends MathClass implements OnInit, OnDestroy {
  @ViewChild(EventCardComponent, {static: false}) yesNoCard: EventCardComponent;
  @ViewChild(SloganCardComponent, {static: false}) sloganCard: SloganCardComponent;
  days: number;
  cards: EventCard[] = [];
  card: EventCard;
  nextCard: EventCard;
  cardType = 'yesNo';
  disableYes = false;

  constructor(
    private renderer: Renderer2,
    private gameService: GameService
  ) {
    super();
  }

  ngOnInit() {
    this.days = this.gameService.getDays();
    this.gameService.cardsSubject
      .pipe(untilDestroyed(this))
      .subscribe((cards: EventCard[]) => {
        this.cards = cards;
        this.card = this.cards.shift();
      });
  }

  ngOnDestroy(): void {
  }

  doLeftEvent() {
    this.checkEndGame();
    if (this.gameService.endGame) {
      return;
    }
    const card = this.yesNoCard.getRef();
    this.renderer.addClass(card.nativeElement, 'out-left');
    setTimeout(() => {
      this.renderer.removeClass(card.nativeElement, 'out-left');
      this.renderer.addClass(card.nativeElement, 'in-right');
      this.getNextCard('no');
    }, 500);
    setTimeout(() => {
      this.renderer.removeClass(card.nativeElement, 'in-right');
    }, 1000);
  }

  doRightEvent() {
    this.checkEndGame();
    if (this.gameService.endGame) {
      return;
    }
    const card = this.yesNoCard.getRef();
    this.renderer.addClass(card.nativeElement, 'out-right');
    setTimeout(() => {
      this.renderer.removeClass(card.nativeElement, 'out-right');
      this.renderer.addClass(card.nativeElement, 'in-left');
      this.getNextCard('yes');
    }, 500);
    setTimeout(() => {
      this.renderer.removeClass(card.nativeElement, 'in-left');
    }, 1000);
  }

  checkEndGame() {
    if (this.days > 0 && this.cards.length > 0) {
      this.gameService.setDays(this.days--);
    } else {
      this.gameService.endGame = true;
      this.getResults();
    }
  }

  getNextCard(event: string) {
    let nextIndex;
    let nextCardId;
    console.log(this.card, this.nextCard);

    if (this.card.hasOwnProperty('next')) {
      nextIndex = MathClass.getRandomInt(0, this.card.next.length - 1);
      nextCardId = this.card.next[nextIndex];
    } else {
      nextIndex = MathClass.getRandomInt(0, this.card[event].next.length - 1);
      nextCardId = this.card[event].next[nextIndex];
      this.updateCandidates(this.card[event]);
      this.updateCoins(this.card[event].coins);
      this.checkCoins(this.card[event].coins);
    }
    console.log(nextIndex);
    this.nextCard = this.gameService.getCardById(nextCardId);
    this.updateCard();
    // this.trashCard();

    if (this.nextCard.hasOwnProperty('slogan')) {
      this.cardType = 'slogan';
    } else {
      this.cardType = 'yesNo';
    }
  }

  updateCoins(coins: number) {
    let gameCoins = this.gameService.getCoins();
    gameCoins += coins;
    this.gameService.setCoins(gameCoins);
  }

  checkCoins(coins: number) {
    this.disableYes = (-1 * coins) > this.gameService.getCoins();
  }

  updateCard() {
    this.card = this.nextCard;
  }

  trashCard() {
    const deleteIndex = this.cards.indexOf(this.nextCard);
    this.cards.splice(deleteIndex, 1);
  }

  updateCandidates(action: EventCardAction) {
    const candidates = this.gameService.getCandidates();
    for (const person of candidates) {
      const trends = person.trends;
      for (const trend of trends) {
        const points = trend.getPoints();
        let cardPoints;
        if (person.you) {
          cardPoints = action.points[trend.type][MathClass.getRandomInt(0, action.points[trend.type].length - 1)];
        } else {
          cardPoints = -1 * action.points[trend.type][MathClass.getRandomInt(0, action.points[trend.type].length - 1)];
          cardPoints = MathClass.getRandomInt(0, cardPoints);
        }
        trend.setPoints(points + cardPoints);
        person.votes += cardPoints;
      }
    }
  }

  selectSlogan(slogan) {
    const candidates = this.gameService.getCandidates();
    for (const person of candidates) {
      const trends = person.trends;
      for (const trend of trends) {
        const points = trend.getPoints();
        let cardPoints;
        if (person.you) {
          cardPoints = slogan.points[trend.type][MathClass.getRandomInt(0, slogan.points[trend.type].length - 1)];
        } else {
          cardPoints = -1 * slogan.points[trend.type][MathClass.getRandomInt(0, slogan.points[trend.type].length - 1)];
          cardPoints = MathClass.getRandomInt(0, cardPoints);
        }
        trend.setPoints(points + cardPoints);
        person.votes += cardPoints;
      }
    }

    this.checkEndGame();
    if (this.gameService.endGame) {
      return;
    }

    const card = this.sloganCard.getRef();
    this.renderer.addClass(card.nativeElement, 'out-right');
    setTimeout(() => {
      this.renderer.removeClass(card.nativeElement, 'out-right');
      this.renderer.addClass(card.nativeElement, 'in-left');
      this.getNextCard('yes');
    }, 500);
    setTimeout(() => {
      this.renderer.removeClass(card.nativeElement, 'in-left');
    }, 1000);
  }

  getResults() {
    this.gameService.getCandidates().sort((a, b) => {
      if (a.votes > b.votes) {
        return 1;
      }
      if (a.votes < b.votes) {
        return -1;
      }
    });
    alert('Кандидат ' + this.gameService.getCandidates()[0].name + ' побеждает в выборах!');
  }
}
