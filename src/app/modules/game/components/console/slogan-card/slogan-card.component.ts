import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {EventCard} from '@models/event-card.model';
import {GameService} from '@app/modules/game/game.service';

@Component({
  selector: 'app-slogan-card',
  templateUrl: './slogan-card.component.html',
  styleUrls: ['./slogan-card.component.scss']
})
export class SloganCardComponent implements OnInit {
  @ViewChild('cardRef', {static: false}) cardRef: ElementRef;
  card: EventCard;
  slogans;
  @Output() onSelectSlogan = new EventEmitter();
  @Input() set setData(data: EventCard) {
    this.card = data;
  }

  constructor(
    private gameService: GameService
  ) {
  }

  ngOnInit() {
    this.gameService.getSlogans();
    this.slogans = this.gameService.getSlogans().filter(slogan => this.card.slogan.indexOf(slogan.id) !== -1);
    console.log(this.slogans);
  }

  getRef() {
    return this.cardRef;
  }

  selectSlogan(slogan: any) {
    this.onSelectSlogan.emit(slogan);
  }
}
