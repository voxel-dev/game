import {Component, Input, OnInit} from '@angular/core';
import {Candidate} from '@models/candidate.model';
import {CandidateService} from '@app/modules/game/components/candidate/candidate.service';
import {GameService} from '@app/modules/game/game.service';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.scss']
})
export class CandidateComponent implements OnInit {
  @Input() person: Candidate;

  constructor(
    private candidateService: CandidateService,
    private gameService: GameService
  ) { }

  ngOnInit() {
    const candidate = this.person;
    this.person = {...this.candidateService.getRandomPerson(), ...this.person};
    this.gameService.updateCandidate(candidate, this.person);
  }

  getPoliticalTypeText(): string {
    return this.candidateService.getPoliticalTypeText(this.person.politicalType);
  }

  getGroup(): string {
    let before = '';
    if (this.person.group !== 'Самовыдвиженец') {
      before = 'Партия: ';
    }
    return before + this.person.group;
  }
}
