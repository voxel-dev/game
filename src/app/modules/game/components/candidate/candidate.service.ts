import {Injectable} from '@angular/core';
import {MathClass} from '@models/math.class';
import {Candidate} from '@models/candidate.model';
import {Trend} from '@models/trend.model';
import {GameService} from '@app/modules/game/game.service';

@Injectable({
  providedIn: 'root'
})
export class CandidateService extends MathClass {
  private maleNames = ['Иван', 'Федор', 'Дмитрий', 'Алексей', 'Александр', 'Семён', 'Илья', 'Павел', 'Андрей', 'Василий'];
  private femaleNames = ['Ирина', 'Мария', 'Светлана', 'Виктория', 'Вера', 'Надежда', 'Любовь', 'Оксана', 'Наталья', 'Юлия'];
  private lastNames = ['Иванов', 'Федоров', 'Петров', 'Попов', 'Борисов', 'Усачёв', 'Соловьёв', 'Семёнов', 'Бобров', 'Панов'];
  private maleSkins = [
    'assets/img/person-1.svg',
    'assets/img/person-2.svg',
    'assets/img/person-3.svg',
    'assets/img/person-5.svg',
  ];
  private femaleSkins = [
    'assets/img/person-4.svg',
    'assets/img/person-6.svg'
  ];
  private groups = {
    0: ['Людской закон', 'ДВП', 'Уравнение', 'Светлое будущее', 'Всадники справедливости'],
    1: ['Красное яблоко', 'Выбор рабочих', 'Завещание вождя', 'Партия Товарищи', 'Всенародное процветание'],
    2: ['Свой путь', 'Решение народа', 'ЛДСП', 'Честный выбор', 'Гражданская свобода'],
    3: ['Статичная страна', 'Ветер стабильности', 'Изолированный успех', 'Проверенные методы', 'Неизменный курс']
  };

  constructor(
    private gameService: GameService
  ) {
    super();
  }

  private getRandomSex(): string {
    const sex = ['male', 'female'];
    return sex[MathClass.getRandomInt(0, 1)];
  }

  private getRandomPoliticalType(): number {
    return MathClass.getRandomInt(0, 3);
  }

  private getRandomName(sex: string): string {
    const firstName = sex === 'male' ?
      this.maleNames[MathClass.getRandomInt(0, this.maleNames.length - 1)] :
      this.femaleNames[MathClass.getRandomInt(0, this.femaleNames.length - 1)];
    const lastName = this.lastNames[MathClass.getRandomInt(0, this.lastNames.length - 1)];
    const end = sex === 'male' ? '' : 'а';
    return firstName + ' ' + lastName + end;
  }

  private getRandomSkin(sex: string): string {
    return sex === 'male' ?
      this.maleSkins[MathClass.getRandomInt(0, this.maleSkins.length - 1)] :
      this.femaleSkins[MathClass.getRandomInt(0, this.femaleSkins.length - 1)];
  }

  private getRandomGroupByType(type: number): string {
    return this.groups[type][MathClass.getRandomInt(0, this.groups[type].length - 1)];
  }

  private getRandomTrends(): Trend[] {
    const people = new Trend('people');
    people.setProgress(25);
    return [
      people,
      new Trend('capitol'),
      new Trend('military'),
      new Trend('foreign'),
    ];
  }

  public getPoliticalTypeText(type: number): string {
    return this.gameService.politicalTypes[type];
  }

  public getRandomPerson(): Candidate {
    const sex = this.getRandomSex();
    const politicalType = this.getRandomPoliticalType();
    return {
      name: this.getRandomName(sex),
      skin: this.getRandomSkin(sex),
      rating: 0,
      votes: 0,
      politicalType: this.getRandomPoliticalType(),
      group: this.getRandomGroupByType(politicalType),
      trends: this.getRandomTrends(),
      you: false
    };
  }
}
