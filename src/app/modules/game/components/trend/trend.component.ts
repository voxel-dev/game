import {Component, Input, OnInit} from '@angular/core';
import {Trend} from '@models/trend.model';
import {TrendConfig, TrendService} from '@app/modules/game/components/trend/trend.service';

@Component({
  selector: 'app-trend',
  templateUrl: './trend.component.html',
  styleUrls: ['./trend.component.scss'],
  providers: [TrendService]
})
export class TrendComponent implements OnInit {
  @Input() data: Trend;
  @Input() config: TrendConfig;
  icons = ['users', 'capitol', 'military', 'flag'];

  constructor(
    private trendService: TrendService
  ) {
  }

  ngOnInit() {
    this.config = {...this.trendService.defaultConfig, ...this.config};
  }

  getColor(): string {
    switch (true) {
      case 30 >= this.data.getProgress():
        return 'red';
      case 70 >= this.data.getProgress():
        return 'yellow';
      default:
        return 'green';
    }
  }

  getIcon(): string {
    return this.icons[this.data.getTrendType()];
  }
}
