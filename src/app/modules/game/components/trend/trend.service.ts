import { Injectable } from '@angular/core';

export interface TrendConfig {
  line?: boolean;
  points?: boolean;
}

@Injectable()
export class TrendService {
  defaultConfig: TrendConfig = {
    line: false,
    points: false
  };

  constructor() { }
}
