import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GameComponent} from './game.component';
import {GameRoutingModule} from '@app/modules/game/game-routing.module';
import {SharedModule} from '@shared/shared.module';
import {ConsoleComponent} from './components/console/console.component';
import {PressComponent} from './components/press/press.component';
import {CandidateComponent} from './components/candidate/candidate.component';
import {PressCardComponent} from './components/press/press-card/press-card.component';
import {TrendComponent} from './components/trend/trend.component';
import {EventCardComponent} from './components/console/event-card/event-card.component';
import { SloganCardComponent } from './components/console/slogan-card/slogan-card.component';


@NgModule({
  declarations: [
    GameComponent,
    ConsoleComponent,
    PressComponent,
    CandidateComponent,
    PressCardComponent,
    TrendComponent,
    EventCardComponent,
    SloganCardComponent
  ],
  imports: [
    CommonModule,
    GameRoutingModule,
    SharedModule
  ]
})
export class GameModule {
}
