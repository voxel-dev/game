import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConfigComponent} from '@app/modules/config/config.component';

const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthGuard],
    component: ConfigComponent,
    children: [
      // {
      //   path: '',
      //   redirectTo: 'hmi',
      //   pathMatch: 'full'
      // },
      // {
      //   path: 'create',
      //   canActivate: [AuthGuard],
      //   component: MatchEditorComponent
      // },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigRoutingModule {
}
